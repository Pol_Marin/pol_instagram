import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css"

import Items from "./Items";


function App() {
  return (
    <div className="App">
      <Items />
    </div>
  );
}

export default App;
