import React, { useEffect, useState } from "react";
import { v4 as uuid } from "uuid";
import {
  Container,
  Col,
  Row,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from "reactstrap";

const Items = () => {

  /* STATES */

  const [items, setItems] = useState([]);
  const [url, setUrl] = useState();
  const [coment, setComent] = useState();

  /* EFFECTS - ejecución automatica del guardado y cargado de datos */

  useEffect(() => {
    recuperar();
  }, []);

  useEffect(() => {
    if (items.length) {
      guardar();
    }
  }, [items]);

  /* GUARDAR DATOS EN LOCAL */

  const guardar = () => {
    localStorage.setItem('mis_items', JSON.stringify(items));
  }

  /* CARGAR DATOS DE LOCAL */

  const recuperar = () => {
    const itemsJson = localStorage.getItem('mis_items');
    const data = JSON.parse(itemsJson);
    if (data && data.length) {
      setItems(data);
    } else {
      setItems([]);
    }
  }

  /* AÑADIR POST (URL IMAGEN & COMENTARIO) */

  const afegir = () => {
    if (url && coment) {
      const nouItem = {
        imagen: url,
        comentario: coment,
        id: uuid(),
        like: 0,
        share: 0,
      };
      setItems([...items, nouItem]);
      setUrl('');
      setComent('');
    }
  };

  /* SUBIR LIKES */

  const sendLikes = (el) => {
    for (let item of items) {
      if (el.id === item.id) {
        el.like = el.like + 1;
      }
    }
    setItems([...items]);
  }

  /* CREAR CARTAS */

  const cartas = items.map((el) => (

    <Col xs="12" md="6" lg="4">
      <div className=" m-1">
        <Card className="carta" key={el.id}>
          <CardImg top className="img-carta" width="50%" src={el.imagen} alt={el.imagen} />
          <CardBody>
            <CardTitle className="comentario">{el.comentario}</CardTitle>
            <div className="likes">
              <i className="fa fa-heart" onClick={() => sendLikes(el)}></i>
              <strong>{el.like}</strong>
            </div>
          </CardBody>
        </Card>
      </div>
    </Col>

  )
  )


  return (
    <>
      <div className="titulo">
        <h1>P<i className="fa fa-instagram"></i>LSTAGRAM</h1>
      </div>
      <div className="cuerpo">
        <Container>
          <div className="introducir">
            <h2>Sube tu post</h2>
            <InputGroup className="m-2">
              <InputGroupAddon addonType="prepend">
                <InputGroupText>URL</InputGroupText>
              </InputGroupAddon>
              <Input type="text" value={url} onChange={(ev) => setUrl(ev.target.value)} placeholder="Introduce la URL de la foto..." />
            </InputGroup>
            <InputGroup className="m-2">
              <InputGroupAddon addonType="prepend">
                <InputGroupText>Titulo</InputGroupText>
              </InputGroupAddon>
              <Input type="text" value={coment} onChange={(ev) => setComent(ev.target.value)} placeholder="Introduce un título..." />
            </InputGroup>
            <div className="botonera">
              <Button className="m-3" onClick={afegir}>AÑADIR</Button>
            </div>
          </div>
          <div className="cart-container">
            <Row>
              {cartas}
            </Row>
          </div>
        </Container>
      </div>
    </>
  );
};

export default Items;
